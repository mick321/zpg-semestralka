﻿varying vec3 Normal;
varying vec3 CamDirection;
varying vec3 LightDirection[2];

varying vec4 MatAmbient[2];
varying vec4 MatDiffuse[2];
varying vec4 MatSpecular[2];

varying vec4 color;
varying vec2 uv;

varying vec4 shadPos;

uniform sampler2D tex0, shadowTex;
uniform float shadowIntensity;

void main() {

    // alpha testing
    vec4 texColor = texture(tex0, uv.xy);
    if (texColor.a - 0.5f < 0)
	    discard;
    
    // rozptylene cteni ze shadowmapy
	vec2 poissonDisk[4] = vec2[4](
	  vec2( -0.94201624, -0.39906216 ),
	  vec2( 0.94558609, -0.76890725 ),
	  vec2( -0.094184101, -0.92938870 ),
	  vec2( 0.34495938, 0.29387760 )
	);
	// offset proti shadow acne
	float shadowBias = 0.003f;	

    vec4 intensity;	
	float diffuse, specular;
	
	// nocni svetlo	
	diffuse = max(dot(normalize(Normal),normalize(LightDirection[1])),0.0);
	specular = 
		pow(max(
				dot(
					reflect(normalize(LightDirection[1]),normalize(Normal)),
					-normalize(CamDirection)
					)
				,0
			), gl_FrontMaterial.shininess);
	
	intensity += (MatAmbient[1] + MatDiffuse[1] * diffuse + MatSpecular[1] * specular);	
	
	// denni svetlo	
	diffuse = max(dot(normalize(Normal),normalize(LightDirection[0])),0.0);
	specular = 
		pow(max(
				dot(
					reflect(normalize(LightDirection[0]),normalize(Normal)),
					-normalize(CamDirection)
					)
				,0
			), gl_FrontMaterial.shininess);
				
	vec3 shadPosNorm = shadPos.xyz / shadPos.w;	
	float len = length(shadPosNorm.xy);
	shadPosNorm *= 0.5f;
	shadPosNorm += vec3(0.5f, 0.5f, 0.5f);
	
	// kreslime stiny jen tam, kam "dosahne" shadowmapa
	if (len < 1.0f)
	{
	    float shade = 0.0f;
		for (int i = 0; i < 4; i++)
		   shade += shadowIntensity * step(shadowBias, shadPosNorm.z - texture(shadowTex, shadPosNorm.xy + poissonDisk[i]/1000.f).x);

		float add = len * len * (4 * shadowIntensity);
		diffuse *= clamp(1.0f - shade + add, 0.0f, 1.0f);
		specular *= clamp(1.0f - shade + add, 0.0f, 1.0f);
	}
	
	intensity += (MatAmbient[0] + MatDiffuse[0] * diffuse + MatSpecular[0] * specular);	
		
	intensity *= texColor * color;
	
	gl_FragColor = intensity;
}