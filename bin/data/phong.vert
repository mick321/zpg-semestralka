﻿varying vec3 Normal;
varying vec3 CamDirection;
varying vec3 LightDirection[2];

varying vec4 MatAmbient[2];
varying vec4 MatDiffuse[2];
varying vec4 MatSpecular[2];

varying vec4 color;
varying vec2 uv;

varying vec4 shadPos;

uniform mat4 WorldMatrix;
uniform mat4 ShadowsMatrix;

void main() {
	CamDirection= (gl_ModelViewMatrix * gl_Vertex).xyz;
	LightDirection[0] = (gl_LightSource[0].position).xyz;
	LightDirection[1] = (gl_LightSource[1].position).xyz;
	MatAmbient[0] = gl_LightSource[0].ambient * gl_FrontMaterial.ambient;
	MatAmbient[1] = gl_LightSource[1].ambient * gl_FrontMaterial.ambient;
	MatDiffuse[0] = gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse;
	MatDiffuse[1] = gl_LightSource[1].diffuse * gl_FrontMaterial.diffuse;
	MatSpecular[0] = gl_LightSource[0].specular * gl_FrontMaterial.specular;
	MatSpecular[1] = gl_LightSource[1].specular * gl_FrontMaterial.specular;	
	color = gl_Color;
	uv = gl_MultiTexCoord0.xy;
	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
	Normal = gl_NormalMatrix * gl_Normal;
	
	shadPos = ShadowsMatrix * WorldMatrix * gl_Vertex;
}