﻿varying vec2 uv;
varying vec4 position;

uniform sampler2D tex0;

void main() {    

  vec4 texColor = texture(tex0, uv.xy);
	if (texColor.a < 0.99)
	    discard;

	gl_FragColor.x = 0.5f + 0.5f * position.z / position.w;	
}