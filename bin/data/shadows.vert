﻿
varying vec2 uv;
varying vec4 position;

void main() {     
	position = gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
	uv = gl_MultiTexCoord0.xy;
}