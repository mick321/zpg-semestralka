\select@language {czech}
\contentsline {section}{\numberline {1}Zad\'an\IeC {\'\i }}{3}
\contentsline {subsection}{\numberline {1.1}Situace}{3}
\contentsline {subsection}{\numberline {1.2}Implementace}{3}
\contentsline {subsection}{\numberline {1.3}Po\v zadavky}{3}
\contentsline {section}{\numberline {2}Sc\'ena}{4}
\contentsline {section}{\numberline {3}Ter\'en}{4}
\contentsline {section}{\numberline {4}Pohyb po ter\'enu}{5}
\contentsline {subsection}{\numberline {4.1}Kamera}{5}
\contentsline {subsection}{\numberline {4.2}Horizont\'aln\IeC {\'\i } pohyb}{5}
\contentsline {subsection}{\numberline {4.3}Konstantn\IeC {\'\i } v\'y\v ska kamery nad ter\'enem}{6}
\contentsline {section}{\numberline {5}Osv\v etlen\IeC {\'\i } sc\'eny}{7}
\contentsline {section}{\numberline {6}Popis ovl\'ad\'an\IeC {\'\i }}{7}
\contentsline {section}{\numberline {7}Implementovan\'a roz\v s\IeC {\'\i }\v ren\IeC {\'\i }}{7}
\contentsline {subsection}{\numberline {7.1}Obarven\IeC {\'\i } ter\'enu}{7}
\contentsline {subsection}{\numberline {7.2}Kamen\IeC {\'\i }}{7}
\contentsline {subsection}{\numberline {7.3}Vegetace (stromy)}{7}
\contentsline {subsection}{\numberline {7.4}Shadow mapping}{7}
\contentsline {section}{\numberline {8}Z\'av\v er}{8}
