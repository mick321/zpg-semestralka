﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenGL4NET;
using System.Runtime.InteropServices;
using System.Drawing;

namespace Zpg
{
    /// <summary>
    /// Spolecne rozhrani pro vsechna graficka data.
    /// </summary>
    abstract class GraphicsData
    {
        int id;
        static int idCounter = 1;

        public static uint lastTexture = 0;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public GraphicsData()
        {
            id = idCounter;
            idCounter++;
        }

        /// <summary>
        /// Metoda pro nacteni grafickych dat.
        /// </summary>
        /// <returns></returns>        
        public abstract Boolean Load(String filename);

        /// <summary>
        /// Uvolneni dat vyuzivanych OpenGL.
        /// </summary>
        public abstract void Destroy();

        /// <summary>
        /// Aktualizace grafickych dat.
        /// </summary>
        public abstract void Update(GDInstance instance);

        /// <summary>
        /// Vykresleni grafickych dat.
        /// </summary>
        public abstract void Draw();

        /// <summary>
        /// Bounding box grafickych dat.
        /// </summary>
        /// <returns></returns>
        public abstract BoundingBox GetBoundingBox();

        /// <summary>
        /// Ziskani identifikatoru zdroje.
        /// </summary>
        /// <returns>id zdroje</returns>
        public int GetResourceId()
        {
            return id;
        }
    }

    // ------------------------------------------------------------------------------------------------

    /// <summary>
    /// Trida "instance" grafickych dat. Lze si ji predstavit jako konkretni umisteni dat v prostoru.
    /// </summary>
    class GDInstance : IComparable
    {
        GraphicsData data;
        public Boolean UseLighting;

        /// <summary>
        /// Jednoznacny identifikator
        /// </summary>
        private int id;

        /// <summary>
        /// Ziskani jednoznacneho identifikatoru.
        /// </summary>
        /// <returns>id instance</returns>
        public int GetId()
        {
            return id;
        }

        /// <summary>
        /// Ziskani vnitrnich dat.
        /// </summary>
        /// <returns></returns>
        public GraphicsData GetData()
        {
            return data;
        }

        static int idCounter = 1;
        
        /// <summary>
        /// Matice transformace objektu.
        /// </summary>
        public Matrix matrix;

        public static int worldMatrixUniform;

        /// <summary>
        /// Konstruktor instance grafickeho objektu.
        /// </summary>
        public GDInstance()
        {
            UseLighting = true;
            matrix = new Matrix();
            matrix.SetIdentity();

            id = idCounter;
            idCounter++;
        }

        /// <summary>
        /// Pripojeni grafickych dat k instanci.
        /// </summary>
        /// <param name="data">Reference na graficka data.</param>
        public void SetGraphicsData(GraphicsData data)
        {
            this.data = data;            
        }

        /// <summary>
        /// Aktualizace instance.
        /// </summary>
        public void Update()
        {
            if (data != null)
                data.Update(this);
        }

        public void MatrixIdentity()
        {
            matrix.SetIdentity();
        }

        public void MatrixTranslate(float x, float y, float z)
        {
            matrix.Translate(x, y, z);
        }

        public void MatrixRotate(float angle, float axisx, float axisy, float axisz)
        {
            matrix.Rotate(angle, axisx, axisy, axisz);
        }

        /// <summary>
        /// Vykresleni instance.
        /// </summary>
        public void Draw(bool shadows = false)
        {
            if (data != null)
            {
                gl.PushMatrix();
                gl.MultMatrixf(matrix);

                if (UseLighting)
                    gl.Enable(GL.LIGHTING);
                else
                    gl.Disable(GL.LIGHTING);

                if (!shadows)
                    gl.UniformMatrix4fv(worldMatrixUniform, 1, false, matrix);

                data.Draw();

                gl.PopMatrix();
            }
        }

        /// <summary>
        /// Porovnani objektu (kvuli razeni ve fronte kresleni)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            GDInstance o = (GDInstance)obj;
            if (o == null)
                return 0;

            int rtn = 0;
            
            if (id > o.GetId())
                rtn += 1;
            else if (id < o.GetId())
                rtn -= 1;

            if (data != null && o.GetData() != null)
            {
                int r1, r2;
                r1 = data.GetResourceId();
                r2 = o.GetData().GetResourceId();
                if (r1 > r2)
                    rtn += 2;
                else if (r1 < r2)
                    rtn -= 2;
            }

            return rtn;
        }
    }

    // ------------------------------------------------------------------------------------------------

    /// <summary>
    /// Knihovna grafickych dat.
    /// </summary>
    class Resources
    {
        SortedDictionary<String, GraphicsData> dictionary;

        /// <summary>
        /// Konstruktor, inicializace knihovny.
        /// </summary>
        public Resources()
        {
            dictionary = new SortedDictionary<String, GraphicsData>();
        }

        /// <summary>
        /// Ulozeni grafickych dat do knihovny pod danym klicem.
        /// </summary>
        /// <param name="key">klic</param>
        /// <param name="data">reference na graficka data</param>
        /// <returns>Vraci false, pokud je klic jiz vyuzity</returns>
        public Boolean Store(String key, GraphicsData data)
        {
            if (dictionary.ContainsKey(key))
                return false;
            dictionary.Add(key, data);
            return true;
        }

        /// <summary>
        /// Vrati graficka data ulozena na zadanem klici.
        /// </summary>
        /// <param name="key">klic</param>
        /// <returns>reference na graficka data, null v pripade, ze klic neni vyuzivany</returns>
        public GraphicsData Fetch(String key)
        {
            if (dictionary.ContainsKey(key))
                return dictionary[key];

            return null;
        }

        /// <summary>
        /// Likvidace polozek.
        /// </summary>
        public void Destroy()
        {
            foreach (KeyValuePair<String, GraphicsData> item in dictionary)
            {
                item.Value.Destroy();
            }
        }

        /// <summary>
        /// Nacteni textury a vygenerovani mipmap - u dlazdic muze mit za dusledek artefakty.
        /// </summary>
        /// <param name="address"></param>
        public static void MipmappedTexImage2D(String address)
        {
            Bitmap bitmap = new Bitmap(address);
            int w = bitmap.Width;
            int h = bitmap.Height;

            int level = 0;
            while (true)
            {                
                gl.TexImage2D(GL.TEXTURE_2D, level, bitmap);
                
                if (w == 1 && h == 1)
                    break;

                level++;

                if (w > 1)
                    w /= 2;
                if (h > 1)  
                    h /= 2;
                bitmap = ResizeBitmap(bitmap, w, h);

            }
        }

        /// <summary>
        /// Nacteni textury a jejich mipmap z ruznych souboru. Lze vyuzit pro ruzne ucely.
        /// </summary>
        /// <param name="address"></param>
        public static void ManuallyMipmappedTexImage2D(String[] address)
        {
            for (int i = 0; i < address.Length; i++)
            {
                gl.TexImage2D(GL.TEXTURE_2D, i, address[i]);
            }

            gl.TexParameteri(GL.TEXTURE_2D, GL.TEXTURE_BASE_LEVEL, 0);
            gl.TexParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAX_LEVEL, address.Length - 1);
        }

        /// <summary>
        /// Zmena velikosti bitmapy pro automaticky generovane urovne textury. 
        /// Prevzato ze stackoverflow.com.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private static Bitmap ResizeBitmap(Bitmap image, int width, int height)
        {
            //a holder for the result
            Bitmap result = new Bitmap(width, height);            

            // set the resolutions the same to avoid cropping due to resolution differences
            result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            //use a graphics object to draw the resized image into the bitmap
            using (Graphics graphics = Graphics.FromImage(result))
            {
                //set the resize quality modes to high quality                                
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                
                //draw the image into the target bitmap
                graphics.DrawImage(image, 0, 0, result.Width, result.Height);
            }

            //return the resulting bitmap
            return result;

        }    
    }

    // ---------------------------------------------------------------------------------------------------

    /// <summary>
    /// Trida grafickych utilit.
    /// </summary>
    static class Utils
    {
        /// <summary>
        /// Nalezeni kolmeho vektoru k zadanemu vektoru v prostoru.
        /// Nestabilni na okoli vektoru [1,0,0] a [-1,0,0], nicmene pro nase ucely staci.
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        public static Vector3 FindOrthonormalizedVector(Vector3 vec)
        {
            Vector3 rtn = new Vector3(vec.x, vec.y, vec.z);
            rtn.x = 0f;
            float y = rtn.y;
            rtn.y = -rtn.z;
            rtn.z = y;
            rtn.Normalize();
            return rtn;
        }
    }

    // ---------------------------------------------------------------------------------------------------

    struct BoundingBox
    {
        public float x1, y1, z1;
        public float x2, y2, z2;

        /// <summary>
        /// Uplny konstruktor.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="z1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="z2"></param>
        public BoundingBox(float x1, float y1, float z1, float x2, float y2, float z2)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.z1 = z1;
            this.x2 = x2;
            this.y2 = y2;
            this.z2 = z2;
        }
        
    }

    // ---------------------------------------------------------------------------------------------------

    /// <summary>
    /// Zaznam jednoho vrcholu otexturovaneho spritu.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    struct SpriteVertex
    {
        /// <summary>
        /// souradnice vrcholu
        /// </summary>
        public float x, y, z;
        public float u, v;

        /// <summary>
        /// Uplny konstruktor.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="u"></param>
        /// <param name="v"></param>
        public SpriteVertex(float x, float y, float z, float u, float v)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.u = u;
            this.v = v;
        }

        /// <summary>
        /// Velikost struktury v bytech
        /// </summary>
        public static int SizeInBytes
        {
            get
            {
                return Marshal.SizeOf(typeof(SpriteVertex));
            }
        }
    }
}
