﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenGL4NET;

namespace Zpg
{
    /// <summary>
    /// Trida hrace - de facto "ovladac" kamery.
    /// </summary>
    class Player
    {
        const float PLAYER_HEIGHT = 1.85f;
        const float PLAYER_SPEED = 3f;
        const float ROTATION_SENSITIVITY = 0.3f;

        Scene scene;
        Vector2 currentPosition;
        Vector2 currentRotation;
        Vector2 currentSpeed;

        float invertY = 1f;

        /// <summary>
        /// Konstruktor tridy hrace.
        /// </summary>
        /// <param name="scene"></param>
        public Player(Scene scene, Vector2 defaultPosition)
        {
            this.scene = scene;
            currentPosition = new Vector2(defaultPosition.x, defaultPosition.y);
            currentRotation = new Vector2();
            currentSpeed = new Vector2();
        }

        /// <summary>
        /// Metoda zajistujici pohyb hrace po scene.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="up"></param>
        /// <param name="down"></param>
        /// <param name="sprint"></param>
        public void Move(Boolean left, Boolean right, Boolean up, Boolean down, Boolean sprint)
        {
            if (left || right || up || down)
            {
                float csX = 0f, csY = 0f;

                if (left)
                    csX -= 1f;
                if (right)
                    csX += 1f;
                if (up)
                    csY -= 1f;
                if (down)
                    csY += 1f;

                currentSpeed.x = (float)Math.Cos(currentRotation.y * Math.PI / 180f) * csX
                    - (float)Math.Sin(currentRotation.y * Math.PI / 180f) * csY;
                currentSpeed.y = (float)Math.Sin(currentRotation.y * Math.PI / 180f) * csX
                    + (float)Math.Cos(currentRotation.y * Math.PI / 180f) * csY;
                
                const float uglyEps = 0.01f;
                if (currentSpeed.Length > uglyEps)
                {
                    currentSpeed.Normalize();
                    currentSpeed.Mult(sprint ? (PLAYER_SPEED * 3f) : (PLAYER_SPEED));
                }
            }
            else
            {
                currentSpeed.x = 0f;
                currentSpeed.y = 0f;
            }
        }

        /// <summary>
        /// Ovladani rotace hrace.
        /// </summary>
        /// <param name="deltaRotX"></param>
        /// <param name="deltaRotY"></param>
        public void Rotate(float deltaRotX, float deltaRotY)
        {
            currentRotation.x += deltaRotX * ROTATION_SENSITIVITY * invertY;
            if (currentRotation.x < -90f)
                currentRotation.x = -90f;
            else if (currentRotation.x > 90f)
                currentRotation.x = 90f;

            currentRotation.y += deltaRotY * ROTATION_SENSITIVITY;
            if (currentRotation.y < 0f)
                currentRotation.y += 360f;
            if (currentRotation.y > 360f)
                currentRotation.y -= 360f;
        }
        
        /// <summary>
        /// Aplikuje hrace na kameru, zkontroluje pripadne kolize atd.
        /// </summary>
        public void Apply(float frameTime)
        {
            Camera c = scene.GetCamera();
            Terrain t = (Terrain)scene.GetResources().Fetch("terrain");
            if (t == null)
                return;

            Vector2 storedPosition;
            storedPosition = new Vector2(currentPosition.x, currentPosition.y);
            currentPosition += currentSpeed * frameTime;
            if (scene.ReadCollision(currentPosition.x, currentPosition.y))
            {
                currentPosition.x = storedPosition.x;
                currentPosition.y = storedPosition.y;
            }

            if (currentPosition.x < 0f)
                currentPosition.x = 0f;
            if (currentPosition.y < 0f)
                currentPosition.y = 0f;
            if (currentPosition.x > t.GetRealWidth())
                currentPosition.x = t.GetRealWidth();
            if (currentPosition.y > t.GetRealHeight())
                currentPosition.y = t.GetRealHeight();

            c.position.x = currentPosition.x;            
            c.position.z = currentPosition.y;
            c.position.y = t.GetHeight(c.position.x, c.position.z) + PLAYER_HEIGHT;

            c.rotX = -currentRotation.x;
            c.rotY = 360f - currentRotation.y;
        }

        /// <summary>
        /// Vypne/zapne invertovane ovladani.
        /// </summary>
        public void ToggleInvertY()
        {
            invertY = -invertY;
        }

    }
}
