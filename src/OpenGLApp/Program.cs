﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using OpenGL4NET;
using System.Drawing;

namespace Zpg
{
    /// <summary>
    /// Aplikacni trida, obsluhuje udalosti (klavesnice + mys), komunikuje se scenou.
    /// </summary>
    class OpenGLApp : Form
    {
        RenderingContext rc;
        Boolean[] keys;
        Boolean lockCursor;
        Boolean displayFrameTimes;
        Point defaultCursorPos;

        Scene scene;
        Player player;

        Time time;
        /// <summary>
        /// Konstruktor aplikacni tridy.
        /// </summary>
        public OpenGLApp()
        {
            keys = new Boolean[256];
            // Nastaveni velikosti okna
            ClientSize = new System.Drawing.Size(800, 600);
            // Vytvoreni kontextu.
            rc = RenderingContext.CreateContext(this, new RenderingContextSetting() { multisample = 255 });

            Location = new Point(10, 10); 
            Text = "Semestrální práce ze ZPG";
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpenGLApp));
            Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        
            // VSync
            if (wgl.Extension.isEXT_swap_control)
                wgl.SwapInterval(1);

            if (!gl.Extension.isARB_vertex_buffer_object) 
                throw new Exception("Rozšíření ARB_vertex_buffer_object není podporováno, program bude ukončen.");
            if (!gl.Extension.isEXT_vertex_array) 
                throw new Exception("Rozšíření EXT_vertex_array není podporováno, program bude ukončen.");

            Cursor.Hide();
            lockCursor = true;
            defaultCursorPos = new Point();
            defaultCursorPos.X = this.Left + this.Size.Width / 2;
            defaultCursorPos.Y = this.Top + this.Size.Height / 2;
            Cursor.Position = defaultCursorPos;

            time = new Time();
            displayFrameTimes = false;
        }

        /// <summary>
        /// Inicializace aplikacni tridy.
        /// Jedna se napriklad o vytvoreni sceny, jeji inicializaci atd.
        /// </summary>
        /// <returns>Vraci, zda byla inicializace uspesna.</returns>
        public Boolean Init()
        {
            // vytvoreni sceny
            scene = new Scene(rc);
            if (!scene.Init())
                return false;

            // vytvoreni a umisteni hrace
            Terrain t = (Terrain)scene.GetResources().Fetch("terrain");            

            Vector2 playerDefaultPosition = new Vector2(t.GetRealWidth() * 0.5f, t.GetRealHeight() * 0.5f);
            player = new Player(scene, playerDefaultPosition);

            scene.OnWindowResize(ClientSize.Width, ClientSize.Height);

            return true;
        }

        /// <summary>
        /// Zajisteni dealokace zdroju pouzivanych OpenGL.
        /// </summary>
        public void Destroy()
        {
            if (scene != null)
                scene.Destroy();
        }

        /// <summary>
        /// Metoda pro vykonani ukolu uvnitr hlavni smycky.
        /// Slouzi napr. k aktualizaci a vykresleni sceny.
        /// </summary>
        public void Work()
        {            
            time.NextFrame();
            player.Move(keys[(int)Keys.A], keys[(int)Keys.D], keys[(int)Keys.W], keys[(int)Keys.S], keys[(int)Keys.ShiftKey]);
            if (lockCursor && this == ActiveForm)
            {
                defaultCursorPos.X = this.Left + this.Size.Width / 2;
                defaultCursorPos.Y = this.Top + this.Size.Height / 2;

                player.Rotate(Cursor.Position.Y - defaultCursorPos.Y, Cursor.Position.X - defaultCursorPos.X);

                Cursor.Position = defaultCursorPos;
            }

            player.Apply(time.LastFrameDuration * 0.001f);

            scene.Update(time);
            scene.Draw(time);

            if (displayFrameTimes)
                Console.WriteLine("frame time: " + time.LastFrameDuration);
        }

        /// <summary>
        /// Osetreni zmeny velikosti okna.
        /// </summary>
        /// <param name="e">parametry udalosti</param>
        protected override void OnResize(EventArgs e)
        {
            if (scene != null)
                scene.OnWindowResize(ClientSize.Width, ClientSize.Height);

            base.OnResize(e);
        }

        /// <summary>
        /// Osetreni stisku klavesy.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            int index = (int)e.KeyCode;
            if (index >= 0 && index <= 255)
                keys[index] = true;
            base.OnKeyDown(e);

            if (e.KeyCode == Keys.U)
                player.ToggleInvertY();

            if (keys[(int)Keys.ControlKey] && e.KeyCode == Keys.F)
                displayFrameTimes = !displayFrameTimes;

            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        /// <summary>
        /// Osetreni uvolneni klavesy.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            int index = (int)e.KeyCode;
            if (index >= 0 && index <= 255)
                keys[index] = false;
            base.OnKeyDown(e);
        }

        /// <summary>
        /// Vstupni bod programu.
        /// </summary>
        /// <param name="args">parametry programu</param>
        static void Main(string[] args)
        {
            OpenGLApp app = new OpenGLApp();            

            if (app.Init())
            {
                app.Show();
                app.Activate();

                // Tento zpusob neni zdaleka nejlepsi, ale nejjednodussi.
                // Problem je, ze metoda DoEvents() alokuje s kazdym zavolanim spoustu
                // pameti, kterou bude muset GC pozdeji uvolnit.
                // Pro zajemce např. 
                // - blog Toma Millera http://blogs.msdn.com/b/tmiller/
                // - http://code.dawnofthegeeks.com/2009/05/07/c-lesson-1-the-main-loop/
                // - http://bobobobo.wordpress.com/2009/06/12/game-loop-in-c/
                while (app.Created)
                {
                    app.Work();
                    Application.DoEvents();
                }                
            }
            app.Destroy();
            Environment.Exit(0);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpenGLApp));
            this.SuspendLayout();
            // 
            // OpenGLApp
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);           
            this.ResumeLayout(true);

        }
    }
}
