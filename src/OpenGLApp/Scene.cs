﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenGL4NET;
using System.IO;

namespace Zpg
{
    /// <summary>
    /// Trida, ktera se stara o rendering. Spravuje objekty a jejich instance, teren, oblohu a kameru.
    /// </summary>
    class Scene
    {
        // reference na rendering context
        RenderingContext rc;
        Resources resources;
        Sky sky;
        Camera camera;
        SortedSet<GDInstance> instances;

        Program phong, shadows;
        Matrix shadowsMatrixObject;

        const int SHADOWMAP_SIZE_X = 1024, SHADOWMAP_SIZE_Y = 1024;
        const float SHADOWMAP_REAL_SIZE = 40.0f;
        uint shadowFrameBuffer, shadowDepthBuffer, shadowMap;
        int shadowsMatrix, shadowsU0, shadowsU1, shadowIntensity;

        bool[] collisionMap;
        int collisionSizeX;
        int collisionSizeZ;
        int collisionPrecision;

        /// <summary>
        /// Konstruktor sceny.
        /// </summary>
        /// <param name="rc">reference na rendering context</param>
        public Scene(RenderingContext rc)
        {
            this.rc = rc;
            resources = new Resources();
            camera = new Camera();
            sky = new Sky();
            instances = new SortedSet<GDInstance>();
            collisionMap = null;
            collisionPrecision = 2;
        }

        /// <summary>
        /// Generate objects in scene.
        /// </summary>
        private Boolean GenerateObjects()
        {
            Random r = new Random();

            Terrain teren = (Terrain)resources.Fetch("terrain");
            // rozmisteni kamenu
            for (int i = 0; i < 50; i++)
            {
                GDInstance inst = new GDInstance();
                GraphicsData stone = resources.Fetch("stone" + r.Next(5));
                inst.SetGraphicsData(stone);
                float x, y, z;

                BoundingBox bb = stone.GetBoundingBox();

                bool ok = false;
                do
                {
                    x = (float)r.NextDouble() * teren.GetRealWidth();
                    z = (float)r.NextDouble() * teren.GetRealHeight();

                    y = teren.GetHeight(x, z);

                    // vylouceni mista startu
                    if (x + bb.x1 <= 1 && x + bb.x2 >= -1 && z + bb.z1 <= 1 && z + bb.z2 >= -1)
                        continue;

                    // vylouceni jiz obsazenych mist
                    if (ReadCollisionRect(x + bb.x1 * 3, z + bb.z1 * 3, x + bb.x2 * 3, z + bb.z2 * 3))
                        continue;

                    float y_1, y_2, y_3, y_4;
                    y_1 = teren.GetHeight(x + 2, z);
                    y_2 = teren.GetHeight(x, z + 2);
                    y_3 = teren.GetHeight(x - 2, z);
                    y_4 = teren.GetHeight(x, z - 2);

                    // povolime umisteni jen na rovinkach
                    float maxuheltan = (float)Math.Tan(1.0 * 180 / Math.PI) / 2.0f;
                    if (Math.Abs(y_1 - y) < maxuheltan && Math.Abs(y_2 - y) < maxuheltan &&
                        Math.Abs(y_3 - y) < maxuheltan && Math.Abs(y_4 - y) < maxuheltan)
                    {
                        WriteCollisionRect(x + bb.x1, z + bb.z1, x + bb.x2, z + bb.z2, true);
                        ok = true;
                    }
                } while (!ok);

                inst.MatrixTranslate(x, y - 0.2f, z);                
                AddObjectInstance(inst);
            }

            // rozmisteni stromu
            for (int i = 0; i < 50; i++)
            {            
                GDInstance inst = new GDInstance();
                GraphicsData tree = resources.Fetch("tree" + r.Next(5));
                inst.SetGraphicsData(tree);

                float x, y, z;

                BoundingBox bb = tree.GetBoundingBox();

                bool ok = false;
                do
                {
                    x = (float)r.NextDouble() * teren.GetRealWidth();
                    z = (float)r.NextDouble() * teren.GetRealHeight();

                    y = teren.GetHeight(x, z);

                    // vylouceni mista startu
                    if (x + bb.x1 <= 1 && x + bb.x2 >= -1 && z + bb.z1 <= 1 && z + bb.z2 >= -1)
                        continue;

                    // vylouceni jiz obsazenych mist
                    if (ReadCollisionRect(x + bb.x1 * 3, z + bb.z1 * 3, x + bb.x2 * 3, z + bb.z2 * 3))
                        continue;

                    float y_1, y_2, y_3, y_4;
                    y_1 = teren.GetHeight(x + 2, z);
                    y_2 = teren.GetHeight(x, z + 2);
                    y_3 = teren.GetHeight(x - 2, z);
                    y_4 = teren.GetHeight(x, z - 2);

                    // povolime umisteni jen na rovinkach
                    float maxuheltan = (float)Math.Tan(1.0 * 180 / Math.PI) / 2.0f;
                    if (Math.Abs(y_1 - y) < maxuheltan && Math.Abs(y_2 - y) < maxuheltan &&
                        Math.Abs(y_3 - y) < maxuheltan && Math.Abs(y_4 - y) < maxuheltan)
                    {
                        WriteCollisionRect(x + bb.x1, z + bb.z1, x + bb.x2, z + bb.z2, true);
                        ok = true;
                    }
                } while (!ok);


                inst.MatrixTranslate(x, y - 0.2f, z);
                inst.MatrixRotate(-10.0f + 20.0f * (float)r.NextDouble(), 0, 0, 1);
                inst.MatrixRotate(((int)(x * y * z)) % 360, 0, 1, 0);                
                AddObjectInstance(inst);
            }

            return true;
        }

        /// <summary>
        /// Inicializace sceny, pri ktere se vytvareji graficke objekty.
        /// </summary>
        /// <returns>vraci true, pokud se inicializace povedla</returns>
        public Boolean Init()
        {
            gl.CullFace(GL.BACK);
            gl.Enable(GL.CULL_FACE);
            gl.Enable(GL.DEPTH_TEST);
            gl.Enable(GL.DEPTH_WRITEMASK);
            gl.ShadeModel(GL.SMOOTH);

            //gl.PolygonMode(GL.FRONT_AND_BACK, GL.LINE);

            // nacteni terenu
            Terrain terrain = new Terrain();
            if (!terrain.Load("data/terrain.raw"))            
                return false;            
            resources.Store("terrain", terrain);

            // generovani kamenu
            for (int i = 0; i < 5; i++)
            {
                Stone stone = new Stone();
                if (!stone.Load(""))
                    return false;
                resources.Store("stone" + i, stone);
            }

            // generovani stromu
            for (int i = 0; i < 5; i++)
            {
                Tree tree = new Tree();
                if (!tree.Load(""))
                    return false;
                resources.Store("tree" + i, tree);
            }

            // vytvoreni kolizni mapy
            collisionSizeX = (int)(terrain.GetRealWidth() * collisionPrecision);
            collisionSizeZ = (int)(terrain.GetRealWidth() * collisionPrecision);
            collisionMap = new bool[collisionSizeX * collisionSizeZ];
            System.Array.Clear(collisionMap, 0, collisionSizeX * collisionSizeZ);
            
            // pridani instance terenu na scenu
            GDInstance inst = new GDInstance();
            inst.SetGraphicsData(terrain);
            AddObjectInstance(inst);

            // pridani dalsich instanci v terenu
            GenerateObjects();

            // vytvoreni shadowmapy
            shadowDepthBuffer = gl.GenRenderBuffer();
            gl.BindRenderbuffer(GL.RENDERBUFFER, shadowDepthBuffer);
            gl.RenderbufferStorage(GL.RENDERBUFFER, GL.DEPTH_COMPONENT, SHADOWMAP_SIZE_X, SHADOWMAP_SIZE_Y);
            gl.BindRenderbuffer(GL.RENDERBUFFER, 0);

            uint[] id = new uint[1];
            gl.GenFramebuffers(1, id);
            shadowFrameBuffer = id[0];
            gl.BindFramebuffer(GL.FRAMEBUFFER, shadowFrameBuffer);
            gl.FramebufferRenderbuffer(GL.FRAMEBUFFER, GL.DEPTH_ATTACHMENT, GL.RENDERBUFFER, shadowDepthBuffer);

            shadowMap = gl.GenTexture();
            gl.BindTexture(GL.TEXTURE_2D, shadowMap);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
            gl.TexImage2D(GL.TEXTURE_2D, 0, GL.R32F, SHADOWMAP_SIZE_X, SHADOWMAP_SIZE_Y, 0, GL.RGBA, GL.UNSIGNED_BYTE, (IntPtr)0);
            gl.FramebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, shadowMap, 0);
            gl.BindTexture(GL.TEXTURE_2D, 0);

            if (gl.CheckFramebufferStatus(GL.FRAMEBUFFER) != GL.FRAMEBUFFER_COMPLETE) 
                Console.WriteLine("nepovedlo se vytvorit framebuffer"); 
            gl.BindFramebuffer(GL.FRAMEBUFFER, 0);

            // nacteni shaderu
            phong = new Program("phong", File.ReadAllText("data/phong.vert"), File.ReadAllText("data/phong.frag"));
            Console.WriteLine(phong.log);
            shadowsMatrix = gl.GetUniformLocation(phong.id, "ShadowsMatrix");
            shadowsU0 = gl.GetUniformLocation(phong.id, "tex0");
            shadowsU1 = gl.GetUniformLocation(phong.id, "shadowTex");
            shadowIntensity = gl.GetUniformLocation(phong.id, "shadowIntensity");
            GDInstance.worldMatrixUniform = gl.GetUniformLocation(phong.id, "WorldMatrix");
            
            shadows = new Program("shadows", File.ReadAllText("data/shadows.vert"), File.ReadAllText("data/shadows.frag"));            
            Console.WriteLine(shadows.log);
            
            gl.Materialf(GL.FRONT, GL.SHININESS, 1.0f);
            
            return true;
        }

        /// <summary>
        /// Uvolneni grafickych objektu, ktere pouziva OpenGL.
        /// </summary>
        public void Destroy()
        {
            phong = null;
            shadows = null;

            gl.DeleteTexture(shadowMap);
            gl.DeleteFramebuffer(shadowFrameBuffer);
            gl.DeleteRenderbuffer(shadowDepthBuffer);

            sky.Destroy();
            resources.Destroy();
        }

        /// <summary>
        /// Update sceny.
        /// </summary>
        public void Update(Time time)
        {
            foreach (GDInstance i in instances)
            {                
                i.Update();
            }
        }

        /// <summary>
        /// Vyrenderovani shadowmapy.
        /// </summary>
        private void RenderShadowMap()
        {
            // ano, noc je mene hardwarove narocna :)
            if (sky.GetDayLerp() < 0.25f || sky.GetDayLerp() > 0.75f)
                return;

            gl.BindFramebuffer(GL.FRAMEBUFFER, shadowFrameBuffer);
            gl.Clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

            gl.Viewport(0, 0, SHADOWMAP_SIZE_X, SHADOWMAP_SIZE_Y);

            gl.MatrixMode(GL.PROJECTION);
            gl.LoadIdentity();
            gl.Ortho(-SHADOWMAP_REAL_SIZE, SHADOWMAP_REAL_SIZE, SHADOWMAP_REAL_SIZE, -SHADOWMAP_REAL_SIZE, -50f, 100f);
            //Matrix p = Matrix.Ortho(-SHADOWMAP_REAL_SIZE, SHADOWMAP_REAL_SIZE, SHADOWMAP_REAL_SIZE, -SHADOWMAP_REAL_SIZE, -50f, 100f);

            double[] pF = new double[16];
            gl.GetDoublev(GL.PROJECTION_MATRIX, pF);
            Matrix p = new Matrix();
            p.m11 = (float)pF[0];
            p.m22 = (float)pF[5];
            p.m33 = (float)pF[10];
            p.m43 = (float)pF[14];
            p.m44 = (float)pF[15];            

            gl.MatrixMode(GL.MODELVIEW);
            gl.LoadIdentity();

            gl.CullFace(GL.FRONT);

            // tuto matici pak preposlat do vertex shaderu druheho pruchodu (pro porovnani)
            Vector3 fromVec = camera.position + sky.publicSunPosition;
            Vector3 toVec = camera.position;
            Vector3 upVec = new Vector3(0, 1, 0);
            Matrix m = Matrix.LookAt(ref fromVec, ref toVec, ref upVec);            
            gl.MultMatrixf(m);
            p.Mult(ref m);

            shadowsMatrixObject = p;
            
            gl.Enable(GL.DEPTH_TEST);

            GraphicsData.lastTexture = 0;

            gl.UseProgram(shadows.id);
            foreach (GDInstance i in instances)
            {
                i.Draw(true);                
            }
            gl.UseProgram(0);

            gl.CullFace(GL.BACK);
            
            gl.BindFramebuffer(GL.FRAMEBUFFER, 0);
            OnWindowResize(0, 0); // obnoveni viewportu            
        }


        /// <summary>
        /// Vyrenderovani pripravene sceny.
        /// </summary>
        public void Draw(Time time)
        {            
            RenderShadowMap();
            gl.Clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

            camera.ApplyOnlyRotations();
            sky.OnFrameBegin(time);            
            camera.Apply();

            GraphicsData.lastTexture = 0;

            gl.UseProgram(phong.id);
            gl.UniformMatrix4fv(shadowsMatrix, 1, false, shadowsMatrixObject);
            
            gl.Uniform1i(shadowsU0, 0);
            gl.Uniform1i(shadowsU1, 1);
            gl.Uniform1f(shadowIntensity, Math.Max(0.0f, 0.3f * sky.publicSunPosition.y));

            gl.ActiveTexture(GL.TEXTURE1);
            gl.Enable(GL.TEXTURE_2D);
            gl.BindTexture(GL.TEXTURE_2D, shadowMap);

            gl.ActiveTexture(GL.TEXTURE0);
            
            foreach (GDInstance i in instances)
            {                
                i.Draw();
            }
            gl.UseProgram(0);

            gl.ActiveTexture(GL.TEXTURE1);
            gl.Disable(GL.TEXTURE_2D);
            gl.ActiveTexture(GL.TEXTURE0);

            rc.SwapBuffers();
        }

        /// <summary>
        /// Zaregistrovani objektu pro renderovani.
        /// </summary>
        /// <param name="objInstance">instance objektu</param>
        public void AddObjectInstance(GDInstance objInstance)
        {
            instances.Add(objInstance);
        }

        int lastWindowW, lastWindowH;

        /// <summary>
        /// Udalost, ktera nastava pri zmene velikosti okna.
        /// </summary>
        /// <param name="w">sirka okna</param>
        /// <param name="h">vyska okna</param>
        public void OnWindowResize(int w, int h)
        {
            if (w == 0)
            {
                gl.Viewport(0, 0, lastWindowW, lastWindowH);
                return;
            }
            lastWindowW = w;
            lastWindowH = h;
            gl.Viewport(0, 0, w, h);
            if (camera != null)
                camera.aspect = (float)w / h;
        }

        /// <summary>
        /// Interni metoda pro zapsani jednoho bodu kolize.
        /// </summary>
        /// <param name="x">kolizni souradnice x</param>
        /// <param name="z">kolizni souradnice z</param>
        /// <param name="occupied">urcuje, jestli je misto obsazene</param>
        private void WriteCollision(int x, int z, bool occupied)
        {
            if (collisionMap == null)
                return;

            if (x < 0 || x >= collisionSizeX)
                return;
            if (z < 0 || z >= collisionSizeZ)
                return;
            
            collisionMap[z * collisionSizeZ + x] = occupied;
        }

        /// <summary>
        /// Metoda pro zapsani obdelniku do kolizni mapy.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="z1"></param>
        /// <param name="x2"></param>
        /// <param name="z2"></param>
        /// <param name="occupied">urcuje, jestli chceme obdelnik zapisovat, nebo mazat</param>
        public void WriteCollisionRect(float x1, float z1, float x2, float z2, bool occupied)
        {
            if (collisionMap == null)
                return;

            int ix1, iz1, ix2, iz2;

            ix1 = (int)(x1 * collisionPrecision);
            iz1 = (int)(z1 * collisionPrecision);

            ix2 = (int)(x2 * collisionPrecision);
            iz2 = (int)(z2 * collisionPrecision);

            for (int i = ix1; i <= ix2; i++)
                for (int j = iz1; j <= iz2; j++)
                    WriteCollision(i, j, occupied);
        }

        /// <summary>
        /// Cteni z kolizni mapy.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        /// <returns>True, pokud je v danem miste kolize</returns>
        public bool ReadCollision(float x, float z)
        {
            if (collisionMap == null)
                return false;

            int ix, iz;
            ix = (int)(x * collisionPrecision);
            if (ix < 0 || ix >= collisionSizeX)
                return false;

            iz = (int)(z * collisionPrecision);
            if (iz < 0 || iz >= collisionSizeZ)
                return false;
                        
            return collisionMap[iz * collisionSizeZ + ix];
        }

        /// <summary>
        /// Cteni z kolizni mapy (obdelnik).
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="z1"></param>
        /// <returns>True, pokud je v danem obdelniku kolize</returns>
        public bool ReadCollisionRect(float x1, float z1, float x2, float z2)
        {
            if (collisionMap == null)
                return false;
            
            int ix1, iz1;
            int ix2, iz2;
            ix1 = Math.Max(0, (int)(x1 * collisionPrecision));            
            iz1 = Math.Max(0, (int)(z1 * collisionPrecision));                        
            ix2 = Math.Min((int)(x2 * collisionPrecision), collisionSizeX - 1);             
            iz2 = Math.Min((int)(z2 * collisionPrecision), collisionSizeZ - 1);             
            
            for (int i =  ix1; i <= ix2; i++)
                for (int j = iz1; j <= iz2; j++)
                {
                    if (collisionMap[j * collisionSizeZ + i])
                        return true;
                }                

            return false;
        }

        /// <summary>
        /// Ziskani reference na kameru.
        /// </summary>
        /// <returns>reference na kameru</returns>
        public Camera GetCamera()
        {
            return camera;
        }

        /// <summary>
        /// Getter na instanci tridy Resources.
        /// </summary>
        /// <returns>reference na tridu zdroju</returns>
        public Resources GetResources()
        {
            return resources;
        }
    }

    // ---------------------------------------------------------------------------------------------------

    /// <summary>
    /// Trida kamery (ovladani pohledu na scenu).
    /// </summary>
    class Camera
    {
        public Vector3 position;
        public float rotX, rotY;
        public float aspect;
        public float fov;

        private Matrix cameraMatrix;

        public Camera()
        {
            aspect = 1f;
            fov = 45f;
            rotX = 0f;
            rotY = 0f;
            position = new Vector3();
            cameraMatrix = new Matrix();
        }

        public void Apply()
        {            
            gl.MatrixMode(GL.PROJECTION);
            gl.LoadMatrixf(Matrix.Perspective(fov, aspect, 0.3f, 180f));
            gl.MatrixMode(GL.MODELVIEW);
            cameraMatrix.SetIdentity();
            cameraMatrix.Rotate(-rotX, 1f, 0f, 0f);
            cameraMatrix.Rotate(-rotY, 0f, 1f, 0f);
            cameraMatrix.Translate(-position.x, -position.y, -position.z);
            gl.LoadMatrixf(cameraMatrix);
        }

        public void ApplyOnlyRotations()
        {
            gl.MatrixMode(GL.PROJECTION);
            gl.LoadMatrixf(Matrix.Perspective(fov, aspect, 0.3f, 180f));
            gl.MatrixMode(GL.MODELVIEW);
            cameraMatrix.SetIdentity();
            cameraMatrix.Rotate(-rotX, 1f, 0f, 0f);
            cameraMatrix.Rotate(-rotY, 0f, 1f, 0f);
            gl.LoadMatrixf(cameraMatrix);
        }

        public Matrix GetMatrix()
        {
            return cameraMatrix;
        }
    }
}
