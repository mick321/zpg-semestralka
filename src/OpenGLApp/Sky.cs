﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenGL4NET;
using System.Windows.Forms;

namespace Zpg
{
    /// <summary>
    /// Trida spravujici oblohu a globalni svetla.
    /// </summary>
    /// LIGHT0 = denni svetlo (slunce)
    /// LIGHT1 = nocni svetlo
    class Sky
    {
        /// <summary>
        /// Den trva dve minuty.
        /// </summary>
        private static float DAY_LENGTH = 120f;

        /// <summary>
        /// Zemepisna sirka oblasti, v niz se nachazime.
        /// Zaporne cislo = severni zemepiska sirka, kladne cislo = jizni zemepisna sirka.
        /// </summary>
        private static float LATITUDE = -50f;

        private static Vector3 SKY_COLOR_NIGHT = new Vector3(30f / 255f, 45f / 255f, 75f / 255f);
        private static Vector3 SKY_COLOR_SUNRISE = new Vector3(165f / 255f, 181f / 255f, 220f / 255f);
        private static Vector3 SKY_COLOR_DAY = new Vector3(135f / 255f, 206f / 255f, 250f / 255f);
        private static Vector3 SKY_COLOR_SUNSET = new Vector3(175f / 255f, 181f / 255f, 220f / 255f);        

        private float dayLerp;
        private Vector3 currentSkyColor;

        public Vector3 publicSunPosition;

        Sun sun;
        
        /// <summary>
        /// Konstruktor oblohy.
        /// </summary>
        public Sky()
        {
            dayLerp = 0.5f; // mame prave poledne!
            currentSkyColor = new Vector3();

            gl.Enable(GL.LIGHT0);
            gl.Lightf(GL.LIGHT0, GL.CONSTANT_ATTENUATION, 0);
            gl.Lightf(GL.LIGHT0, GL.LINEAR_ATTENUATION, 0f);
            gl.Lightf(GL.LIGHT0, GL.QUADRATIC_ATTENUATION, 0f);

            gl.Enable(GL.LIGHT1);
            gl.Lightfv(GL.LIGHT1, GL.POSITION, new float[4] { 0f, -1f, 0f, 0f });
            gl.Lightf(GL.LIGHT1, GL.CONSTANT_ATTENUATION, 0);
            gl.Lightf(GL.LIGHT1, GL.LINEAR_ATTENUATION, 0f);
            gl.Lightf(GL.LIGHT1, GL.QUADRATIC_ATTENUATION, 0f);

            sun = new Sun();
            sun.Init();
        }

        /// <summary>
        /// Pomocna staticka metoda pro linearni interpolaci.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="o"></param>
        /// <param name="t"></param>
        static void lerp(Vector3 v1, Vector3 v2, ref Vector3 o, float t)
        {
            o.x = v1.x * (1f - t) + v2.x * t;
            o.y = v1.y * (1f - t) + v2.y * t;
            o.z = v1.z * (1f - t) + v2.z * t;
        }

        /// <summary>
        /// Metoda spoustena na zacatku kazdeho snimku. Spocita barvu oblohy a intenzitu a smer svetel.
        /// </summary>
        /// <param name="t"></param>
        public void OnFrameBegin(Time t)
        {
            dayLerp += t.LastFrameDuration * 0.001f / DAY_LENGTH; // zde se muze kumulovat chyba delky dne, nicmene v tomto pripade to asi neni problem
            if (dayLerp > 1f)
                dayLerp -= 1f;

            SetSkyColor();
            SetLights();

            sun.Draw();
        }        

        /// <summary>
        /// Uvolneni zdroju.
        /// </summary>
        public void Destroy()
        {
            sun.Destroy();
        }

        /// <summary>
        /// Vypocet barvy oblohy.
        /// </summary>
        private void SetSkyColor()
        {
            float dayTime = dayLerp * 24f;

            if (dayTime < 5.0f || dayTime > 19.0f)
            {
                currentSkyColor.x = SKY_COLOR_NIGHT.x;
                currentSkyColor.y = SKY_COLOR_NIGHT.y;
                currentSkyColor.z = SKY_COLOR_NIGHT.z;
            }
            else if (dayTime >= 5.0f && dayTime < 6.0f)
            {
                lerp(SKY_COLOR_NIGHT, SKY_COLOR_SUNRISE, ref currentSkyColor, (dayTime - 5.0f) * 1.0f);
            }
            else if (dayTime >= 6.0f && dayTime < 6.5f)
            {
                lerp(SKY_COLOR_SUNRISE, SKY_COLOR_DAY, ref currentSkyColor, (dayTime - 6.0f) * 2.0f);
            }
            else if (dayTime >= 6.5f && dayTime < 17.5f)
            {
                currentSkyColor.x = SKY_COLOR_DAY.x;
                currentSkyColor.y = SKY_COLOR_DAY.y;
                currentSkyColor.z = SKY_COLOR_DAY.z;
            }
            else if (dayTime >= 17.5f && dayTime < 18.0f)
            {
                lerp(SKY_COLOR_DAY, SKY_COLOR_SUNSET, ref currentSkyColor, (dayTime - 17.5f) * 2.0f);
            }
            else if (dayTime >= 18.0f && dayTime < 19.0f)
            {
                lerp(SKY_COLOR_SUNSET, SKY_COLOR_NIGHT, ref currentSkyColor, (dayTime - 18.0f) * 1f);
            }

            gl.ClearColor(currentSkyColor.x, currentSkyColor.y, currentSkyColor.z, 1f);
        }

        /// <summary>
        /// Nastaveni svetel na scene podle denni doby.
        /// </summary>
        private void SetLights()
        {
            Vector3 sunPosition = new Vector3((float)Math.Sin(dayLerp * 2f * Math.PI), -(float)Math.Cos(dayLerp * 2f * Math.PI), 0f);
            // matice slouzici k simulaci zemepisne sirky
            // predpoklada se samozrejme rovnodennost (nase planeta treba nema vychylenou osu nebo je 21.3. / 23.9.)
            Matrix sunRot = Matrix.Rotation(-LATITUDE, 1f, 0f, 0f);
            sunPosition.Mult(ref sunRot);

            publicSunPosition = new Vector3(sunPosition.x, sunPosition.y, sunPosition.z);

            sun.SetPosition(dayLerp, LATITUDE);
            
            // denni svetlo
            Vector3 sunVector = new Vector3(-sunPosition.x, Math.Min(0f, -sunPosition.y), -sunPosition.z);
            sunVector.Normalize();            

            gl.Lightfv(GL.LIGHT0, GL.POSITION, new float[4] { sunVector.x, sunVector.y, sunVector.z, 0f} );
            float lightIntensity = (float)Math.Max(Math.Min(10f * (sunPosition.y + 0.14f), 1.0), 0.0);
            float[] lightColor = new float[] { lightIntensity, lightIntensity, lightIntensity, 1f };            
            gl.Lightfv(GL.LIGHT0, GL.DIFFUSE, lightColor);
            gl.Lightfv(GL.LIGHT0, GL.SPECULAR, lightColor);
            lightColor[0] *= 0.5f;
            lightColor[1] *= 0.5f;
            lightColor[2] *= 0.5f;
            gl.Lightfv(GL.LIGHT0, GL.AMBIENT, lightColor);
            
            // nocni svetlo            
            lightIntensity = 1f - lightIntensity;
            lightColor = new float[] { lightIntensity * 0.2f, lightIntensity * 0.35f, lightIntensity * 0.6f, 1f };
            gl.Lightfv(GL.LIGHT1, GL.DIFFUSE, lightColor);
            gl.Lightfv(GL.LIGHT1, GL.SPECULAR, lightColor);
            lightColor[0] *= 0.01f;
            lightColor[1] *= 0.01f;
            lightColor[2] *= 0.01f;
            gl.Lightfv(GL.LIGHT1, GL.AMBIENT, lightColor);            
        }

        /// <summary>
        /// Getter pro LATITUDE.
        /// </summary>
        /// <returns></returns>
        public float GetLatitude()
        {
            return LATITUDE;
        }

        /// <summary>
        /// Getter pro dayLerp.
        /// </summary>
        /// <returns></returns>
        public float GetDayLerp()
        {
            return dayLerp;
        }
    }

    /// <summary>
    /// Slunce a jeho kresleni.
    /// </summary>
    class Sun
    {
        uint vertexVBO;
        uint indexVBO;

        uint sunTexture;

        float dayLerp, latitude;

        public Sun()
        {

        }

        public void SetPosition(float dayLerp, float latitude)
        {
            this.dayLerp = dayLerp;
            this.latitude = latitude;
        }

        public Boolean Init()
        {
            Boolean result = true;
            SpriteVertex[] verts = new SpriteVertex[4];
            verts[0] = new SpriteVertex(-1, 0, -1, 0, 0);
            verts[1] = new SpriteVertex(1, 0, -1, 1, 0);
            verts[2] = new SpriteVertex(-1, 0, 1, 0, 1);
            verts[3] = new SpriteVertex(1, 0, 1, 1, 1);
            
            vertexVBO = gl.GenBuffer();
            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.BufferData(GL.ARRAY_BUFFER, 6 * SpriteVertex.SizeInBytes, verts, GL.STATIC_DRAW);

            uint[] indices = new uint[] { 0, 2, 1, 2, 3, 1 };
            indexVBO = gl.GenBuffer();
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);
            gl.BufferData(GL.ELEMENT_ARRAY_BUFFER, 6 * sizeof(uint), indices, GL.STATIC_DRAW);

            gl.BindBuffer(GL.ARRAY_BUFFER, 0);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);

            sunTexture = gl.GenTexture();                                   
            gl.BindTexture(GL.TEXTURE_2D, sunTexture);
            try
            {
                gl.TexImage2D(GL.TEXTURE_2D, 0, "data/sun.png");
            }
            catch
            {
                MessageBox.Show("Nepodařilo se načíst texturu slunce! Slunce bude zobrazováno bez textury.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.REPEAT);    
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.REPEAT);
            gl.BindTexture(GL.TEXTURE_2D, 0);

            return result;
        }

        public void Destroy()
        {
            if (vertexVBO != 0)
                gl.DeleteBuffer(vertexVBO);
            vertexVBO = 0;

            if (indexVBO != 0)
                gl.DeleteBuffer(indexVBO);
            indexVBO = 0;

            if (sunTexture != 0)
                gl.DeleteTexture(sunTexture);
            sunTexture = 0;
        }

        /// <summary>
        /// Vykresleni slunce
        /// </summary>
        public void Draw()
        {
            gl.PushAttrib(GL.ENABLE_BIT);
            gl.Disable(GL.DEPTH_TEST);
            gl.Disable(GL.LIGHTING);
            gl.Disable(GL.CULL_FACE);
            gl.PushMatrix();

            gl.Rotatef(-latitude, 1f, 0f, 0f);
            gl.Rotatef(dayLerp * 360f, 0f, 0f, 1f);
            gl.Translatef(0f, -7f, 0f);

            // priprava
            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.EnableClientState(GL.VERTEX_ARRAY);
            gl.EnableClientState(GL.TEXTURE_COORD_ARRAY);
            gl.VertexPointer(3, GL.FLOAT, SpriteVertex.SizeInBytes, (IntPtr)0);
            gl.TexCoordPointer(2, GL.FLOAT, SpriteVertex.SizeInBytes, (IntPtr)12);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);

            gl.Enable(GL.BLEND);
            gl.BlendFunc(GL.SRC_COLOR, GL.ONE);
            gl.ActiveTexture(GL.TEXTURE0);
            gl.Enable(GL.TEXTURE_2D);
            gl.BindTexture(GL.TEXTURE_2D, sunTexture);
            
            // vykresleni
            gl.DrawElements(GL.TRIANGLES, 6, GL.UNSIGNED_INT, (IntPtr)0);
            
            gl.BindTexture(GL.TEXTURE_2D, 0);

            // odpojeni bufferu, ukonceni modu GL_VERTEX_ARRAY            
            gl.DisableClientState(GL.VERTEX_ARRAY);
            gl.DisableClientState(GL.TEXTURE_COORD_ARRAY);
            gl.BindBuffer(GL.ARRAY_BUFFER, 0);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);

            gl.PopMatrix();
            gl.PopAttrib();
        }
    }
}
