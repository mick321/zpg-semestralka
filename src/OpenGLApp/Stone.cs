﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Windows.Forms;

using OpenGL4NET;
using System.Runtime.InteropServices;

namespace Zpg
{
    class Stone : GraphicsData  
    {
        const int SLICE_COUNT = 11;

        const float DEFAULT_SIZEX = 2.5f;
        const float DEFAULT_SIZEY = 1.6f;
        const float DEFAULT_SIZEZ = 2.5f;

        // vertex, index buffer
        uint vertexVBO;
        uint indexVBO;

        uint stoneTexture;

        // rozmery kamenu
        float sizex, sizey, sizez;        

        /// <summary>
        /// Implicitne definovany konstruktor.
        /// </summary>
        public Stone()
        {            
            Random r = new Random();            
            sizex = Stone.DEFAULT_SIZEX * ((float)r.NextDouble() * 0.75f + 0.5f);
            sizey = Stone.DEFAULT_SIZEY * ((float)r.NextDouble() * 0.8f + 0.4f);
            sizez = Stone.DEFAULT_SIZEZ * ((float)r.NextDouble() * 0.75f + 0.5f);
        }

        /// <summary>
        /// Nacteni kamene - nacita pouze texturu, zbytek se generuje za behu.
        /// </summary>
        /// <param name="filename">nevyuzito</param>
        /// <returns>Vraci true pri uspechu</returns>
        public override Boolean Load(String filename)
        {            
            // vygenerovani vertex a index bufferu
            int i, j;

            StoneVertex[] verts = new StoneVertex[(SLICE_COUNT + 1) * (SLICE_COUNT + 1)];

            Random r = new Random();

            uint k = 0;
            for (i = 0; i <= SLICE_COUNT; i++)
                for (j = 0; j <= SLICE_COUNT; j++, k++)
                {
                    // vrcholy
                    verts[k].x = sizex * (float)Math.Sin((float)j / SLICE_COUNT * 2.0 * Math.PI);
                    verts[k].x *= (float)Math.Sin((float)i / SLICE_COUNT * Math.PI);
                    verts[k].y = sizey * (float)Math.Cos((float)i / SLICE_COUNT * Math.PI);
                    verts[k].z = sizez * (float)Math.Cos((float)j / SLICE_COUNT * 2.0 * Math.PI);
                    verts[k].z *= (float)Math.Sin((float)i / SLICE_COUNT * Math.PI);
                    
                    // normaly
                    verts[k].nx = -(float)Math.Sin((float)j / SLICE_COUNT * 2.0 * Math.PI);
                    verts[k].nx *= (float)Math.Sin((float)i / SLICE_COUNT * Math.PI);
                    verts[k].ny = -(float)Math.Cos((float)i / SLICE_COUNT * Math.PI);
                    verts[k].nz = -(float)Math.Cos((float)j / SLICE_COUNT * 2.0 * Math.PI);
                    verts[k].nz *= (float)Math.Sin((float)i / SLICE_COUNT * Math.PI);

                    // texturove koordinaty
                    verts[k].u = (float)j / (SLICE_COUNT);
                    verts[k].v = (float)i / (SLICE_COUNT);

                    // offsety pozic (realistictejsi kameny)
                    if (i > 0 && i < SLICE_COUNT && j > 0 && j < SLICE_COUNT)
                    {
                        verts[k].x *= 0.8f + 0.2f * (float)r.NextDouble();
                        verts[k].y *= 0.8f + 0.2f * (float)r.NextDouble();
                        verts[k].z *= 0.8f + 0.2f * (float)r.NextDouble();
                    }
                }
            
            vertexVBO = gl.GenBuffer();            
            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.BufferData(GL.ARRAY_BUFFER, (SLICE_COUNT + 1) * (SLICE_COUNT + 1) * StoneVertex.SizeInBytes, verts, GL.STATIC_DRAW);

            // Vytvoreni a naplneni pole indexu
            uint[] indices = new uint[SLICE_COUNT * SLICE_COUNT * 6];
            k = 0;            
            for (i = 0; i < SLICE_COUNT; i++)
                for (j = 0; j < SLICE_COUNT; j++)
                {                    
                    indices[k++] = (uint)(i * (SLICE_COUNT + 1) + j);
                    indices[k++] = (uint)((i + 1) * (SLICE_COUNT + 1) + j);
                    indices[k++] = (uint)((i + 1) * (SLICE_COUNT + 1) + j + 1);

                    indices[k++] = (uint)(i * (SLICE_COUNT + 1) + j);
                    indices[k++] = (uint)((i + 1) * (SLICE_COUNT + 1) + j + 1);
                    indices[k++] = (uint)(i * (SLICE_COUNT + 1) + j + 1);
                }

            // Vygenerovani ID pro buffer indexu
            indexVBO = gl.GenBuffer();
            // Pripojeni a naplneni bufferu indexu
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);
            gl.BufferData(GL.ELEMENT_ARRAY_BUFFER, SLICE_COUNT * SLICE_COUNT * 6 * sizeof(uint), indices, GL.STATIC_DRAW);

            // Odpojeni bufferu
            gl.BindBuffer(GL.ARRAY_BUFFER, 0);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);

            LoadStoneTexture();
            return true;
        }

        public override void Destroy()
        {
            gl.DeleteBuffer(vertexVBO);
            gl.DeleteBuffer(indexVBO);
            gl.DeleteTexture(stoneTexture);            
        }

        public override void Update(GDInstance instance)
        {
            // neni potreba nic menit
        }

        public override void Draw()
        {
            // priprava
            gl.PushAttrib(GL.ENABLE_BIT);

            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.EnableClientState(GL.VERTEX_ARRAY);            
            gl.EnableClientState(GL.NORMAL_ARRAY);
            gl.EnableClientState(GL.TEXTURE_COORD_ARRAY);
            gl.VertexPointer(3, GL.FLOAT, StoneVertex.SizeInBytes, (IntPtr)0);
            gl.NormalPointer(GL.FLOAT, StoneVertex.SizeInBytes, (IntPtr)12);
            gl.TexCoordPointer(2, GL.FLOAT, StoneVertex.SizeInBytes, (IntPtr)24);            
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);


            float[] material = new float[4];
            material[0] = 0.2f;
            material[1] = 0.2f;
            material[2] = 0.2f;
            material[3] = 1.0f;
            gl.Materialfv(GL.FRONT, GL.SPECULAR, material);
            material[0] = 1.0f;
            material[1] = 1.0f;
            material[2] = 1.0f;
            material[3] = 1f;
            gl.Materialfv(GL.FRONT, GL.DIFFUSE, material);
            material[0] = 1.0f;
            material[1] = 1.0f;
            material[2] = 1.0f;
            material[3] = 1f;
            gl.Materialfv(GL.FRONT, GL.AMBIENT, material);            

            gl.ActiveTexture(GL.TEXTURE0);
            gl.Enable(GL.TEXTURE_2D);
            
            if (lastTexture != stoneTexture)
            {
                gl.BindTexture(GL.TEXTURE_2D, stoneTexture);
                lastTexture = stoneTexture;
            }
            
            // vykresleni
            gl.DrawElements(GL.TRIANGLES, SLICE_COUNT * SLICE_COUNT * 6, GL.UNSIGNED_INT, (IntPtr)0);

            // odpojeni bufferu, ukonceni modu GL_VERTEX_ARRAY            
            gl.DisableClientState(GL.VERTEX_ARRAY);            
            gl.DisableClientState(GL.NORMAL_ARRAY);
            gl.DisableClientState(GL.TEXTURE_COORD_ARRAY);
            //gl.BindBuffer(GL.ARRAY_BUFFER, 0);
            //gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);
        }

        public override BoundingBox GetBoundingBox()
        {
            return new BoundingBox(-sizex, -sizey, -sizez, sizex, sizey, sizez);
        }

        private void LoadStoneTexture() 
        {
            // nacteni textury terenu
            stoneTexture = gl.GenTexture();
            gl.BindTexture(GL.TEXTURE_2D, stoneTexture);
            try
            {
                Resources.ManuallyMipmappedTexImage2D(new String[] { "data/stone_tex0.png", "data/stone_tex1.png", 
                    "data/stone_tex2.png", "data/stone_tex3.png"});
            }
            catch
            {
                MessageBox.Show("Nepodařilo se načíst texturu kamene!.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR_MIPMAP_LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.REPEAT);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.REPEAT);
            gl.BindTexture(GL.TEXTURE_2D, 0);
        }
    }

    // ---------------------------------------------------------------------------------------------------

    /// <summary>
    /// Zaznam jednoho vrcholu kamenu.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    struct StoneVertex
    {
        /// <summary>
        /// souradnice vrcholu
        /// </summary>
        public float x, y, z;
        public float nx, ny, nz;
        public float u, v;
        
        /// <summary>
        /// Velikost struktury v bytech
        /// </summary>
        public static int SizeInBytes
        {
            get
            {
                return Marshal.SizeOf(typeof(StoneVertex));
            }
        }
    }
}
