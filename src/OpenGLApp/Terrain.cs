﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Windows.Forms;

using OpenGL4NET;
using System.Runtime.InteropServices;

namespace Zpg
{
    class Terrain : GraphicsData
    {
        
        public const float TERRAIN_SEGMENT_SIZE = 2f;

        public const int DEFAULT_TERRAIN_WIDTH = 128;
        public const int DEFAULT_TERRAIN_HEIGHT = 128;

        // vertex, index buffer
        uint vertexVBO;
        uint indexVBO;

        uint terrainTexture;

        //public static uint tex;

        // rozmery terenu
        int w, h;
        float[] elevation;

        public Terrain()
        {
            w = Terrain.DEFAULT_TERRAIN_WIDTH;
            h = Terrain.DEFAULT_TERRAIN_HEIGHT;
        }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="width">sirka terenu</param>
        /// <param name="height">hloubka terenu</param>
        public Terrain(int width, int height)
        {
            w = width;
            h = height;
        }

        /// <summary>
        /// Nacteni terenu ze souboru v raw formatu.
        /// </summary>
        /// <param name="filename">jmeno souboru</param>
        /// <returns></returns>
        public override Boolean Load(String filename)
        {
            byte[] heights_data;

            // pokus o nacteni vysek ze souboru
            try
            {
                heights_data = File.ReadAllBytes(filename);
                if (heights_data == null)
                    throw new Exception("Chybí pole výšek.");
                if (heights_data.Length != w * h)
                    throw new Exception("Neočekávaný rozměr mapy.");
            }
            catch (Exception e)
            {
                MessageBox.Show("Nepodařilo se načíst výškovou mapu pro terén! Terén bude placatý. (" + e.Message + ")", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                heights_data = new byte[w * h];
            }

            // konverze vysek v RAW formatu na vnitrne pouzivany float
            elevation = new float[w * h];
            for (int i = 0; i < w * h; i++)
                elevation[i] = 0.1f * heights_data[i];
            heights_data = null;      
      
            // vygenerovani vertex a index bufferu

            TerrainVertex[] verts = new TerrainVertex[w * h];
            Random random = new Random();
            for (int j = 0; j < h; j++)
                for (int i = 0; i < w; i++)
                {
                    verts[j * w + i].x = i * TERRAIN_SEGMENT_SIZE;
                    verts[j * w + i].y = elevation[j * w + i];
                    verts[j * w + i].z = j * TERRAIN_SEGMENT_SIZE;

                    Vector3 normal = GetNormal(i, j);
                    verts[j * w + i].nx = normal.x;
                    verts[j * w + i].ny = normal.y;
                    verts[j * w + i].nz = normal.z;

                    verts[j * w + i].u = i * 0.5f;
                    verts[j * w + i].v = j * 0.5f;

                    verts[j * w + i].g = 0.85f + (float)random.NextDouble() * 0.15f - verts[j * w + i].y * 0.025f;
                    verts[j * w + i].r = 0.65f + (float)random.NextDouble() * verts[j * w + i].g * 0.15f;
                    verts[j * w + i].b = 0.65f + (float)random.NextDouble() * verts[j * w + i].r * 0.25f - verts[j * w + i].y * 0.025f;
                }

            vertexVBO = gl.GenBuffer();            
            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.BufferData(GL.ARRAY_BUFFER, w * h * TerrainVertex.SizeInBytes, verts, GL.STATIC_DRAW);

            // Vytvoreni a naplneni pole indexu
            uint[] indices = new uint[(w-1) * (h-1) * 6];
            uint k = 0;
            for (uint j = 0; j < h - 1; j++)
                for (uint i = 0; i < w - 1; i++)
                {
                    indices[k++] = j * (uint)w + i;
                    indices[k++] = (j + 1) * (uint)w + i;
                    indices[k++] = j * (uint)w + i + 1;

                    indices[k++] = j * (uint)w + i + 1;
                    indices[k++] = (j + 1) * (uint)w + i;
                    indices[k++] = (j + 1) * (uint)w + i + 1;
                }

            // Vygenerovani ID pro buffer indexu
            indexVBO = gl.GenBuffer();
            // Pripojeni a naplneni bufferu indexu
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);
            gl.BufferData(GL.ELEMENT_ARRAY_BUFFER, (w - 1) * (h - 1) * 6 * sizeof(uint), indices, GL.STATIC_DRAW);

            // Odpojeni bufferu
            gl.BindBuffer(GL.ARRAY_BUFFER, 0);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);

            LoadTerrainTexture();
            return true;
        }

        public override void Destroy()
        {
            gl.DeleteBuffer(vertexVBO);
            gl.DeleteBuffer(indexVBO);
            gl.DeleteTexture(terrainTexture);            
        }

        public override void Update(GDInstance instance)
        {
            // neni potreba nic menit
        }

        public override void Draw()
        {
            // priprava
            gl.PushAttrib(GL.ENABLE_BIT);
            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.EnableClientState(GL.VERTEX_ARRAY);
            gl.EnableClientState(GL.COLOR_ARRAY);
            gl.EnableClientState(GL.NORMAL_ARRAY);
            gl.EnableClientState(GL.TEXTURE_COORD_ARRAY);
            gl.VertexPointer(3, GL.FLOAT, TerrainVertex.SizeInBytes, (IntPtr)0);
            gl.NormalPointer(GL.FLOAT, TerrainVertex.SizeInBytes, (IntPtr)12);
            gl.TexCoordPointer(2, GL.FLOAT, TerrainVertex.SizeInBytes, (IntPtr)24);
            gl.ColorPointer(3, GL.FLOAT, TerrainVertex.SizeInBytes, (IntPtr)32);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);

            float[] material = new float[4];
            material[0] = 0.5f;
            material[1] = 0.5f;
            material[2] = 0.5f;
            material[3] = 0.5f;
            gl.Materialfv(GL.FRONT, GL.SPECULAR, material);
            material[0] = 1.0f;
            material[1] = 1.0f;
            material[2] = 1.0f;
            material[3] = 1f;
            gl.Materialfv(GL.FRONT, GL.DIFFUSE, material);
            material[0] = 1.0f;
            material[1] = 1.0f;
            material[2] = 1.0f;
            material[3] = 1f;
            gl.Materialfv(GL.FRONT, GL.AMBIENT, material);

            gl.ActiveTexture(GL.TEXTURE0);
            gl.Enable(GL.TEXTURE_2D);
            gl.BindTexture(GL.TEXTURE_2D, terrainTexture);

            //gl.BindTexture(GL.TEXTURE_2D, tex);
            lastTexture = terrainTexture;

            // vykresleni
            gl.DrawElements(GL.TRIANGLES, (w-1)*(h-1)*6, GL.UNSIGNED_INT, (IntPtr)0);

            // odpojeni bufferu, ukonceni modu GL_VERTEX_ARRAY            
            gl.DisableClientState(GL.VERTEX_ARRAY);
            gl.DisableClientState(GL.COLOR_ARRAY);
            gl.DisableClientState(GL.NORMAL_ARRAY);
            gl.DisableClientState(GL.TEXTURE_COORD_ARRAY);
            gl.BindBuffer(GL.ARRAY_BUFFER, 0);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);
        }

        public override BoundingBox GetBoundingBox()
        {
            return new BoundingBox();
        }

        public float GetRealWidth()
        {
            return (w-1) * TERRAIN_SEGMENT_SIZE;
        }

        public float GetRealHeight()
        {
            return (h-1) * TERRAIN_SEGMENT_SIZE;
        }

        private Vector3 GetFaceNormal(int m1, int n1, int m2, int n2, int m3, int n3)
        {
            /// kontrola bodu
            if (m1 < 0 || n1 < 0 || m2 < 0 || n2 < 0 || m3 < 0 || n3 < 0 ||
                m1 >= w || n1 >= h || m2 >= w || n2 >= h || m3 >= w || n3 >= h)
                return new Vector3(0f, 0f, 0f);
            
            Vector3 v2 = new Vector3(m2 - m1, elevation[n2 * w + m2] - elevation[n1 * w + m1], n2 - n1);
            Vector3 v1 = new Vector3(m3 - m1, elevation[n3 * w + m3] - elevation[n1 * w + m1], n3 - n1);
            Vector3 rtn = Vector3.Cross(ref v1, ref v2);
            rtn.Normalize();
            return rtn;
        }

        public Vector3 GetNormal(int m, int n)
        {                        
            Vector3 rtn = new Vector3();
            rtn += GetFaceNormal(m, n, m, n - 1, m - 1, n);
            rtn += GetFaceNormal(m, n, m + 1, n - 1, m, n - 1);
            rtn += GetFaceNormal(m, n, m + 1, n, m + 1, n - 1);
            rtn += GetFaceNormal(m, n, m, n + 1, m + 1, n);
            rtn += GetFaceNormal(m, n, m - 1, n + 1, m, n + 1);
            rtn += GetFaceNormal(m, n, m - 1, n, m - 1, n + 1);
            rtn.Normalize();
            return rtn;
        }

        public float GetHeight(float x, float z)
        {
            x /= TERRAIN_SEGMENT_SIZE;
            z /= TERRAIN_SEGMENT_SIZE;

            if (x < 0)
                x = 0;
            if (z < 0)
                z = 0;
            if (x >= w - 1)
                x = w - 1.01f;
            if (z >= h - 1)
                z = h - 1.01f;

            int ix = (int)x;
            int iz = (int)z;
            
            float px = x - ix;
            float pz = z - iz;

            if (pz <= 1 - px) // "horni" trojuhelnik
            {
                float y1, y2, y3;
                y1 = elevation[iz * w + ix];
                y2 = elevation[(iz + 1) * w + ix];
                y3 = elevation[iz * w + ix + 1];

                float y1y2 = y1 * (1 - pz) + y2 * pz - y1;
                float y1y3 = y1 * (1 - px) + y3 * px - y1;
                return y1 + y1y2 + y1y3;
            }
            else // "dolni" trojuhelnik
            {
                float y1, y2, y3;
                y1 = elevation[(iz + 1) * w + ix + 1];
                y2 = elevation[iz * w + ix + 1];
                y3 = elevation[(iz + 1) * w + ix];

                px = 1 - px;
                pz = 1 - pz;

                float y1y2 = y1 * (1 - pz) + y2 * pz - y1;
                float y1y3 = y1 * (1 - px) + y3 * px - y1;
                return y1 + y1y2 + y1y3;
            }
        }

        private void LoadTerrainTexture() 
        {
            // nacteni textury terenu
            terrainTexture = gl.GenTexture();
            gl.BindTexture(GL.TEXTURE_2D, terrainTexture);
            try
            {
                Resources.ManuallyMipmappedTexImage2D(new String[] { "data/terrain_tex0.png", "data/terrain_tex1.png", "data/terrain_tex2.png", "data/terrain_tex3.png", "data/terrain_tex4.png" });
            }
            catch
            {
                MessageBox.Show("Nepodařilo se načíst texturu terénu!.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR_MIPMAP_LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.REPEAT);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.REPEAT);
            gl.BindTexture(GL.TEXTURE_2D, 0);
        }
    }

    // ---------------------------------------------------------------------------------------------------

    /// <summary>
    /// Zaznam jednoho vrcholu terenu.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    struct TerrainVertex
    {
        /// <summary>
        /// souradnice vrcholu
        /// </summary>
        public float x, y, z;
        public float nx, ny, nz;
        public float u, v;
        public float r, g, b;

        /// <summary>
        /// Velikost struktury v bytech
        /// </summary>
        public static int SizeInBytes
        {
            get
            {
                return Marshal.SizeOf(typeof(TerrainVertex));
            }
        }
    }
}
