﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Windows.Forms;

using OpenGL4NET;
using System.Runtime.InteropServices;

namespace Zpg
{
    class Tree : GraphicsData
    {
        const int GENERATION_MAX_DEPTH = 8;
        
        // vertex buffer
        uint vertexVBO;
        int vertexCount;
        uint TreeTexture;

        uint indexVBO;
        int indexCount;

        Random r;
        
        /// <summary>
        /// Implicitne definovany konstruktor.
        /// </summary>
        public Tree()
        {
            vertexVBO = 0;
            vertexCount = 0;
            indexCount = 0;
            TreeTexture = 0;
            r = new Random(this.GetHashCode() * System.Environment.TickCount);
        }

        /// <summary>
        /// Generovani l-systemu pomoci gramatiky definovane uvnitr teto funkce.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private String GenerateIteration(String s, int iteration)
        {
            StringBuilder rtn = new StringBuilder();

            double p = r.NextDouble();

            const int LEAF_START = 3;
            const int LEAF_PROBABILITY = 2;

            foreach (char c in s)
            {
                switch (c)
                {
                    case 'K':               
                        rtn.Append("*[[FB*F]*F]*FBFL");
                        break;
                    case 'F':                                             
                        if (iteration == GENERATION_MAX_DEPTH || (iteration >= LEAF_START && r.Next(LEAF_PROBABILITY) == 0))
                        {
                            if (p < 0.15)
                                rtn.Append("BF[FB[BFL]L]");
                            else if (p < 0.35)
                                rtn.Append("BF[BFL]");
                            else
                                rtn.Append("BF[FL]");
                                 
                        }                            
                        else
                        {
                            if (p < 0.25)
                                rtn.Append("SF[BF]F");
                            else
                                rtn.Append("B[BF]");
                        }
                         
                        break;

                    default: 
                        rtn.Append(c);
                        break;
                }
            }

            return rtn.ToString();
        }

        /// <summary>
        /// Generovani listu (tri na sebe kolme plochy s alphatexturou).
        /// </summary>
        /// <param name="verts"></param>
        /// <param name="indices"></param>
        /// <param name="pos"></param>
        /// <param name="dir"></param>
        /// <param name="size"></param>
        private void fillLeaf(List<TreeVertex> verts, List<uint> indices, Vector3 pos, Vector3 dir, float size)
        {
            Vector3 tan1, tan2;            

            Vector3 temporaryPos = new Vector3();
            Vector3 temporaryNorm = new Vector3();            

            float sizeReduced = (1.0f + 0.2f * (float)r.NextDouble()) + size;

            temporaryNorm = dir;
            // tri kolme plochy
            for (int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    tan1 = Utils.FindOrthonormalizedVector(dir);
                    tan2 = Vector3.Cross(dir, tan1);                    
                }
                else if (i == 1)
                {
                    tan1 = Utils.FindOrthonormalizedVector(dir);
                    tan2 = dir;                    
                }
                else
                {
                    tan1 = Utils.FindOrthonormalizedVector(dir);
                    tan2 = dir;
                    tan1 = Vector3.Cross(tan1, tan2);                    
                }

                uint start = (uint)verts.Count;

                temporaryPos = pos - sizeReduced * tan1 - sizeReduced * tan2;
                verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, -temporaryNorm.x, -temporaryNorm.y, -temporaryNorm.z, 144f / 256f, 12f / 256f));
                temporaryPos = pos + sizeReduced * tan1 - sizeReduced * tan2;
                verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, -temporaryNorm.x, -temporaryNorm.y, -temporaryNorm.z, 240f / 256f, 12f / 256f));
                temporaryPos = pos + sizeReduced * tan1 + sizeReduced * tan2;
                verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, -temporaryNorm.x, -temporaryNorm.y, -temporaryNorm.z, 240f / 256f, 114f / 256f));
                temporaryPos = pos - sizeReduced * tan1 + sizeReduced * tan2;
                verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, -temporaryNorm.x, -temporaryNorm.y, -temporaryNorm.z, 144f / 256f, 114f / 256f));

                indices.Add(start);
                indices.Add(start + 1);
                indices.Add(start + 2);
                indices.Add(start);
                indices.Add(start + 2);
                indices.Add(start + 1);

                indices.Add(start);
                indices.Add(start + 2);
                indices.Add(start + 3);
                indices.Add(start);
                indices.Add(start + 3);
                indices.Add(start + 2);
            }

        }

        /// <summary>
        /// Vygenerovani jednoho segmentu stromu. Vuci zadane pozici, smeru se vygeneruje obalujici plocha (kmen/vetev).
        /// </summary>
        /// <param name="list"></param>
        /// <param name="pos"></param>
        /// <param name="dir"></param>
        /// <param name="lastDir"></param>
        /// <param name="size"></param>
        private void fillSegment(List<TreeVertex> verts, List<uint> indices, Vector3 pos, Vector3 dir, Vector3 lastDir, float size, float lastSize)
        {
            const int segmentcount = 6; // pocet obvodovych sten teto casti vetve (6 => podstava je sestiuhelnik)
            const float radiusReduction = 0.2f; // pomer polomeru kmene/vetve vuci delce teto casti vetve

            Vector3 norm, norm2;
            norm = Utils.FindOrthonormalizedVector(lastDir);
            norm2 = Vector3.Cross(lastDir, norm);

            uint start = (uint)verts.Count; // pocatecni index pro nasledne generovani index bufferu
            Vector3 temporaryPos = new Vector3();
            Vector3 temporaryNorm = new Vector3();
            // generovani spodniho patra segmentu
            for (int i = 0; i <= segmentcount; i++)
            {                
                float cosfi, sinfi;
                cosfi = (float)Math.Cos(2.0 * Math.PI * i / segmentcount);
                sinfi = (float)Math.Sin(2.0 * Math.PI * i / segmentcount);

                temporaryNorm = cosfi * norm + sinfi * norm2;
                temporaryNorm.Normalize();
                temporaryPos = pos + temporaryNorm * lastSize * lastSize * radiusReduction;
                verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, -temporaryNorm.x, -temporaryNorm.y, -temporaryNorm.z, 0.5f * (float)i / segmentcount, 0f));                
            }
            
            norm = Utils.FindOrthonormalizedVector(dir);
            norm2 = Vector3.Cross(dir, norm);

            // generovani horniho patra segmentu
            for (int i = 0; i <= segmentcount; i++)
            {
                float cosfi, sinfi;
                cosfi = (float)Math.Cos(2.0 * Math.PI * i / segmentcount);
                sinfi = (float)Math.Sin(2.0 * Math.PI * i / segmentcount);

                temporaryNorm = cosfi * norm + sinfi * norm2;
                temporaryNorm.Normalize();
                temporaryPos = pos + dir + temporaryNorm * size * size * radiusReduction;
                verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, -temporaryNorm.x, -temporaryNorm.y, -temporaryNorm.z, 0.5f * (float)i / segmentcount, 1f));
            }

            // generovani spicek (zakonceni segmentu)
            temporaryPos = pos - lastDir * 0.15f;
            verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, lastDir.x, lastDir.y, lastDir.z, 0.5f, 0.1f));
            temporaryPos = pos + dir * 1.15f;
            verts.Add(new TreeVertex(temporaryPos.x, temporaryPos.y, temporaryPos.z, dir.x, dir.y, dir.z, 0.5f, 0.9f));

            // naplneni index bufferu
            for (uint i = 0; i < segmentcount; i++)
            {
                // obalujici plocha
                indices.Add(start + i);
                indices.Add(start + i + 1);
                indices.Add(start + i + 1 + segmentcount + 1);
                
                indices.Add(start + i);
                indices.Add(start + i + 1 + segmentcount + 1);
                indices.Add(start + i + segmentcount + 1);

                // spicky (de facto podstavy)
                indices.Add(start + i);
                indices.Add(start + 2 * (segmentcount + 1));
                indices.Add(start + i + 1);
                
                indices.Add(start + i + 1 + segmentcount + 1);                
                indices.Add(start + 2 * (segmentcount + 1) + 1);
                indices.Add(start + i + segmentcount + 1);
            }
        }

        /// <summary>
        /// Vygenerovani obsahu vertex a index bufferu a jejich nasledne naplneni.
        /// </summary>
        /// <param name="seq">retezec popisujici L-system</param>
        private void fillVBO(String seq)
        {
            // zasobniky pro navraceni
            Stack<Vector3> positionStack = new Stack<Vector3>();
            Stack<Vector3> olddirectionStack = new Stack<Vector3>();
            Stack<float>   scaleStack = new Stack<float>();
            Stack<float>   oldscaleStack = new Stack<float>();            

            // aktualni a minule meritko
            float scale = 1.1f;
            float oldscale = scale;

            // pozice a smer kresliciho kurzoru
            Vector3 pos = new Vector3(0, 0, 0);
            Vector3 dir = new Vector3(0, 1, 0);
            Vector3 olddir = new Vector3(dir.x, dir.y, dir.z);            

            // dynamicky se zvetsujici buffery pro vrcholy a indexy
            List<TreeVertex> verts = new List<TreeVertex>();
            List<uint> indices = new List<uint>();

            // pruchod popisujicim retezcem
            foreach (char c in seq)
            {
                switch (c)
                {
                    // kmen
                    case 'T':

                        dir *= scale;

                        fillSegment(verts, indices, pos, dir, olddir, scale, oldscale);
                        pos += dir * 0.9f;

                        olddir = dir;
                        olddir.Normalize();

                        dir *= scale;
                        dir.x = (float)(dir.x - 0.05f + 0.1f * r.NextDouble());
                        dir.z = (float)(dir.z - 0.05f + 0.1f * r.NextDouble());
                        dir.Normalize();                                                
                        break;
                    // vetev
                    case 'B':
                    case 'F':
                        if (scale > 0.1f)
                        {
                            scale *= 0.95f;

                            dir *= scale;

                            fillSegment(verts, indices, pos, dir, olddir, scale, oldscale);

                            pos += dir;
                            
                            olddir = dir;
                            olddir.Normalize();

                            dir.Normalize();

                            do 
                            {
                                dir.x = (float)(dir.x - 0.5f + 1.0f * r.NextDouble());
                                dir.y = (float)(dir.y - 0.4f + 0.8f * r.NextDouble());
                                dir.z = (float)(dir.z - 0.5f + 1.0f * r.NextDouble());
                                dir.Normalize();
                            } while (Vector3.Dot(olddir, dir) < 0.0f);

                            oldscale = scale;
                        }
                        break;
                    // list
                    case 'L':
                        fillLeaf(verts, indices, pos, dir, (1.1f - scale) * (1.1f - scale));
                        break;
                    case 'S':                                
                        scale *= 0.9f;
                        break;
                    case '*':

                        dir.Normalize();

                        do
                        {
                            dir.x = (float)(dir.x - 0.5f + 1.0f * r.NextDouble());
                            dir.y = (float)(dir.y - 0.4f + 0.8f * r.NextDouble());
                            dir.z = (float)(dir.z - 0.5f + 1.0f * r.NextDouble());
                            dir.Normalize();
                        } while (Vector3.Dot(olddir, dir) < -0.4f);    
      
                        
                        olddir = dir;
                        olddir.Normalize();                            

                        break;
                    case '[':
                        positionStack.Push(new Vector3(pos.x, pos.y, pos.z));
                        olddirectionStack.Push(new Vector3(olddir.x, olddir.y, olddir.z));
                        scaleStack.Push(scale);
                        oldscaleStack.Push(oldscale);
                        break;
                    case ']':
                        pos = positionStack.Pop();
                        olddir = olddirectionStack.Pop();
                        scale = scaleStack.Pop();
                        oldscale = oldscaleStack.Pop();

                        dir.x = dir.x * 0.1f + 0.9f * (float)(-0.5f + r.NextDouble());
                        dir.y = dir.y * 0.1f + 0.9f * (float)(0.6f + 0.6f * r.NextDouble());
                        dir.z = dir.z * 0.1f + 0.9f * (float)(-0.5f + r.NextDouble());                        

                        dir.Normalize();      

                        break;
                }
            }

            // prevedeni na obycejne pole
            TreeVertex[] vertsArray = verts.ToArray();
            vertexCount = vertsArray.Length;
            uint[] indicesArray = indices.ToArray();
            indexCount = indicesArray.Length;

            // naplneni vertex bufferu
            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.BufferData(GL.ARRAY_BUFFER, vertexCount * TreeVertex.SizeInBytes, vertsArray, GL.STATIC_DRAW);
            gl.BindBuffer(GL.ARRAY_BUFFER, 0);

            // naplneni index bufferu
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);
            gl.BufferData(GL.ELEMENT_ARRAY_BUFFER, indexCount * sizeof(uint), indicesArray, GL.STATIC_DRAW);
            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);
        }

        /// <summary>
        /// Nacteni stromu - nacita pouze texturu, zbytek se generuje za behu.
        /// </summary>
        /// <param name="filename">nevyuzito</param>
        /// <returns>Vraci true pri uspechu</returns>
        public override Boolean Load(String filename)
        {            
            // pocatecni sekvence - T predstavuje kmen, K rozboceni vetvi nad kmenem, F prepisovatelnou vetev            
            String sequence = "TTTTTKF";  

            // iterovani
            for (int i = 0; i < GENERATION_MAX_DEPTH; i++)
                sequence = GenerateIteration(sequence, i);

            // generovani bufferu
            vertexVBO = gl.GenBuffer();
            indexVBO = gl.GenBuffer();            
            // naplneni bufferu
            fillVBO(sequence);            
            // nacteni textury
            LoadTreeTexture();
            return true;
        }

        public override void Destroy()
        {
            gl.DeleteBuffer(vertexVBO);
            gl.DeleteBuffer(indexVBO);
            gl.DeleteTexture(TreeTexture);            
        }

        public override void Update(GDInstance instance)
        {
            // neni potreba nic menit
        }        

        /// <summary>
        /// Vykresleni stromu.
        /// </summary>
        public override void Draw()
        {            
            // priprava
            gl.PushAttrib(GL.ENABLE_BIT);

            gl.BindBuffer(GL.ARRAY_BUFFER, vertexVBO);
            gl.EnableClientState(GL.VERTEX_ARRAY);            
            gl.EnableClientState(GL.NORMAL_ARRAY);
            gl.EnableClientState(GL.TEXTURE_COORD_ARRAY);
            gl.VertexPointer(3, GL.FLOAT, TreeVertex.SizeInBytes, (IntPtr)0);
            gl.NormalPointer(GL.FLOAT, TreeVertex.SizeInBytes, (IntPtr)12);
            gl.TexCoordPointer(2, GL.FLOAT, TreeVertex.SizeInBytes, (IntPtr)24);

            gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, indexVBO);
            
            gl.ActiveTexture(GL.TEXTURE0);
            gl.Enable(GL.TEXTURE_2D);

            if (lastTexture != TreeTexture)
            {
                gl.BindTexture(GL.TEXTURE_2D, TreeTexture);
                lastTexture = TreeTexture;
            }
            
            float[] material = new float[4];
            material[0] = 0.1f;
            material[1] = 0.1f;
            material[2] = 0.1f;
            material[3] = 1.0f;
            gl.Materialfv(GL.FRONT, GL.SPECULAR, material);
            material[0] = 0.3f;
            material[1] = 0.3f;
            material[2] = 0.3f;
            material[3] = 1f;
            gl.Materialfv(GL.FRONT, GL.DIFFUSE, material);
            material[0] = 1.0f;
            material[1] = 1.0f;
            material[2] = 1.0f;
            material[3] = 1f;
            gl.Materialfv(GL.FRONT, GL.AMBIENT, material);
            
            // vykresleni
            gl.DrawElements(GL.TRIANGLES, indexCount, GL.UNSIGNED_INT, (IntPtr)0);

            // odpojeni bufferu, ukonceni modu GL_VERTEX_ARRAY            
            gl.DisableClientState(GL.VERTEX_ARRAY);            
            gl.DisableClientState(GL.NORMAL_ARRAY);
            gl.DisableClientState(GL.TEXTURE_COORD_ARRAY);
            //gl.BindBuffer(GL.ARRAY_BUFFER, 0);
            //gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, 0);
        }

        /// <summary>
        /// Ziskani kolizniho AABB.
        /// </summary>
        /// <returns>AABB ve strukture BoundingBox</returns>
        public override BoundingBox GetBoundingBox()
        {
            return new BoundingBox(-1, -1, -1, 1, 1, 1);
        }

        /// <summary>
        /// Nacteni textury.
        /// </summary>
        private void LoadTreeTexture() 
        {            
            // nacteni textury terenu
            TreeTexture = gl.GenTexture();
            gl.BindTexture(GL.TEXTURE_2D, TreeTexture);
            try
            {
                Resources.ManuallyMipmappedTexImage2D(new String[] { "data/tree_tex0.png", "data/tree_tex1.png", 
                    "data/tree_tex2.png", "data/tree_tex3.png"});
            }
            catch
            {
                MessageBox.Show("Nepodařilo se načíst texturu stromu!.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR_MIPMAP_LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.REPEAT);
            gl.TexParameterf(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.REPEAT);
            gl.BindTexture(GL.TEXTURE_2D, 0);            
        }
    }

    // ---------------------------------------------------------------------------------------------------

    /// <summary>
    /// Zaznam jednoho vrcholu stromu.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    struct TreeVertex
    {
        /// <summary>
        /// souradnice vrcholu
        /// </summary>
        public float x, y, z;
        public float nx, ny, nz;
        public float u, v;

        /// <summary>
        /// Uplny konstruktor.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="nx"></param>
        /// <param name="ny"></param>
        /// <param name="nz"></param>
        /// <param name="u"></param>
        /// <param name="v"></param>
        public TreeVertex(float x, float y, float z, float nx, float ny, float nz, float u, float v)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.nx = nx;
            this.ny = ny;
            this.nz = nz;
            this.u = u;
            this.v = v;
        }

        /// <summary>
        /// Velikost struktury v bytech
        /// </summary>
        public static int SizeInBytes
        {
            get
            {
                return Marshal.SizeOf(typeof(TreeVertex));
            }
        }
    }
}
